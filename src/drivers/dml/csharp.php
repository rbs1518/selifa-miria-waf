<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

use RBS\Selifa\RPC\ILanguageDriverObject;
use RBS\Utility\F;

/**
 * Class csharpLDO
 */
class csharpLDO implements ILanguageDriverObject
{
    /**
     * @var array|null
     */
    protected $Options = null;

    /**
     * @var string
     */
    protected $Namespace = '';

    /**
     * @var array
     */
    protected $VarToClass = array();

    #region ILanguageDriverObject implementation
    /**
     * @param array $options
     * @throws Exception
     */
    public function SetOptions($options)
    {
        $this->Options = $options;
        $this->Namespace = F::Extract($options,'namespace','Default');
        $this->VarToClass = F::Extract($options,'vartoclass',array());
        if (!is_array($this->VarToClass))
            throw new Exception('vartoclass specification is not an array.');
    }
    #endregion

    #region Private Functions
    /**
     * @param string $cp
     * @param array $vars
     * @return string
     */
    private function _GenerateClassFromVars($cp,$vars)
    {
        $s = "using System;\nusing System.Collections.Generic;\nusing System.Runtime.Serialization;\n\n";
        $s .= "namespace ".$this->Namespace."\n{\n\t[DataContract]\n\tpublic class ".$cp."\n\t{\n";
        foreach ($vars as $item)
        {
            $isList = (bool)$item['islist'];
            if ($isList)
            {
                $s .= "\t\tprivate List<".$item['type']."> _".$item['name']." = null;\n\n";
                $s .= "\t\t[DataMember]\n\t\tpublic List<".$item['type']."> ".$item['name']."\n";
                $s .= "\t\t{\n\t\t\tget { return _".$item['name']."; }\n";
                $s .= "\t\t\tset { _".$item['name']." = value; }\n\t\t}\n\n";
            }
            else
            {
                $s .= "\t\tprivate ".$item['type']." _".$item['name']." = ".$item['default'].";\n\n";
                $s .= "\t\t[DataMember]\n\t\tpublic ".$item['type']." ".$item['name']."\n";
                $s .= "\t\t{\n\t\t\tget { return _".$item['name']."; }\n";
                $s .= "\t\t\tset { _".$item['name']." = value; }\n\t\t}\n\n";
            }
        }
        $s .= "\t\tpublic ".$cp."() { }\n";
        $s .= "\t}\n}";
        return $s;
    }

    /**
     * @param string $cp
     * @param array $aData
     * @param array $c
     */
    private function _TranslateAssocArrayToClassList($cp,$aData,&$c)
    {
        if (F::IsAssociativeArray($aData))
        {
            $vars = array();
            foreach ($aData as $key => $value)
            {
                if (is_array($value))
                {
                    if (F::IsAssociativeArray($value))
                    {
                        if (isset($this->VarToClass[$key]))
                            $nextCP = $this->VarToClass[$key];
                        else
                            $nextCP = ($cp.'_'.$key);
                        $this->_TranslateAssocArrayToClassList($nextCP,$value,$c);
                        $vars[] = array(
                            'name' => $key,
                            'type' => $nextCP,
                            'default' => 'null',
                            'islist' => false
                        );
                    }
                    else
                    {
                        if (isset($this->VarToClass[$key]))
                            $nextCP = $this->VarToClass[$key];
                        else
                            $nextCP = ($cp.'_'.$key);
                        $this->_TranslateAssocArrayToClassList($nextCP,$value,$c);
                        $vars[] = array(
                            'name' => $key,
                            'type' => $nextCP,
                            'default' => 'null',
                            'islist' => true
                        );
                    }
                }
                else
                {
                    if (is_string($value))
                    {
                        $varType = 'string';
                        $defaultValue = 'String.Empty';
                    }
                    else if (is_bool($value))
                    {
                        $varType = 'bool';
                        $defaultValue = 'false';
                    }
                    else if (is_double($value))
                    {
                        $varType = 'double';
                        $defaultValue = '0.0';
                    }
                    else if (is_float($value))
                    {
                        $varType = 'double';
                        $defaultValue = '0.0';
                    }
                    else if (is_integer($value))
                    {
                        $varType = 'int';
                        $defaultValue = '0';
                    }
                    else
                    {
                        $varType = 'object';
                        $defaultValue = 'null';
                    }
                    $vars[] = array(
                        'name' => $key,
                        'type' => $varType,
                        'default' => $defaultValue,
                        'islist' => false
                    );
                }
            }

            $c[] = array(
                'ClassName' => $cp,
                'Code' => $this->_GenerateClassFromVars($cp,$vars)
            );
        }
        else
        {
            if (count($aData) > 0)
                $this->_TranslateAssocArrayToClassList($cp,$aData[0],$c);
        }
    }
    #endregion

    public function generate_sdp_code($data)
    {
        header('Content-Type: text/plain');

        $cn = F::Extract($this->Options,'cn','GeneratedClass');

        $classes = array();
        $this->_TranslateAssocArrayToClassList($cn,$data,$classes);

        $output = strtolower(trim(F::Extract($this->Options,'output','json')));
        if ($output == 'raw')
        {
            $content = '';
            foreach ($classes as $classItem)
            {
                $content .= ($classItem['Code']."\n\n");
            }
            return $content;
        }
        else
        {
            return array(
                'Classes' => $classes
            );
        }
    }
}
?>