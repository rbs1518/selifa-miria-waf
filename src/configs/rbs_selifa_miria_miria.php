<?php
return array(
    //'BaseURI' => '',
    'CacheRules' => false,
    'ControllerPath' => 'controllers/',
    'ControllerSuffix' => 'Controller',
    'ControllerOptions' => array(
        'default' => array(
            'DefaultLanguage' => 'id',
            'Parser' => 'RBS\Selifa\Template\TGX',
            'TemplatePath' => 'assets/templates/',
            'MasterLayoutFile' => 'master.html',
            'SkipOPTION' => true
        )
    )
);
?>