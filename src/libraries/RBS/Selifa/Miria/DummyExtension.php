<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Miria;

/**
 * Class DummyExtension
 * @package RBS\Selifa\Miria
 */
class DummyExtension implements IMiriaExtension
{
    /**
     * @param array $ruleDefinitions
     * @param array $options
     */
    public function OnRuleDefine(&$ruleDefinitions, &$options)
    {
        // TODO: Implement OnRuleDefine() method.
    }

    /**
     * @param array $parsedRules
     */
    public function OnRuleDefined(&$parsedRules)
    {
        // TODO: Implement OnRuleDefined() method.
    }

    /**
     * @param IMiriaRouting $router
     * @param string $queryString
     * @param string $requestVerb
     */
    public function OnRuleMatch(IMiriaRouting $router, &$queryString, &$requestVerb)
    {
        // TODO: Implement OnRuleMatch() method.
    }

    /**
     * @param IMiriaRouting $router
     * @param string $queryString
     * @param string $requestVerb
     * @param array $routeInfo
     */
    public function OnRuleMatched(IMiriaRouting $router, $queryString, $requestVerb, &$routeInfo)
    {
        // TODO: Implement OnRuleMatched() method.
    }

    /**
     * @param IMiriaRouting $router
     * @param string $queryString
     * @param string $requestVerb
     * @param array $routeInfo
     */
    public function OnUnmatchedRule(IMiriaRouting $router, $queryString, $requestVerb, &$routeInfo)
    {
        // TODO: Implement OnUnmatchedRule() method.
    }

    /**
     * @param string $filePath
     * @param string $className
     * @param string $method
     * @param array $parameters
     */
    public function OnControllerTranslated($filePath, $className, &$method, &$parameters)
    {
        // TODO: Implement OnControllerTranslated() method.
    }

    /**
     * @param IMiriaController $controller
     * @return mixed
     */
    public function OnControllerDispatched(IMiriaController $controller)
    {
        // TODO: Implement OnControllerDispatched() method.
    }

    /**
     * @param string $filePath
     * @param string $className
     * @param string $errorCode
     * @param string $errorMessage
     * @param boolean $handled
     * @return mixed
     */
    public function OnControllerDispatchError($filePath, $className, $errorCode, $errorMessage, &$handled)
    {
        // TODO: Implement OnControllerDispatchError() method.
    }
}
?>