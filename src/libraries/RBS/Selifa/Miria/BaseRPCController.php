<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Miria;
use RBS\Selifa\Core;
use RBS\Selifa\Miria\IMiriaController;
use RBS\Selifa\Miria\IMiriaExtension;
use RBS\Selifa\Miria\IMiriaAuthentication;
use RBS\Selifa\Miria\IMiriaControllerMethodResult;
use SelifaException;
use Exception;
use ReflectionClass;
use ReflectionMethod;

define('RPC_RETURN_BAD_REQUEST',SELIFA_EXCEPTION_START_CODE + 400);
define('RPC_RETURN_UNAUTHORIZED',SELIFA_EXCEPTION_START_CODE + 401);
define('RPC_RETURN_FORBIDDEN',SELIFA_EXCEPTION_START_CODE + 403);
define('RPC_RETURN_NOT_FOUND',SELIFA_EXCEPTION_START_CODE + 404);
define('RPC_RETURN_METHOD_NOT_ALLOWED',SELIFA_EXCEPTION_START_CODE + 405);
define('RPC_RETURN_NOT_ACCEPTABLE',SELIFA_EXCEPTION_START_CODE + 406);
define('RPC_RETURN_TIMEOUT',SELIFA_EXCEPTION_START_CODE + 408);
define('RPC_RETURN_CONFLICT',SELIFA_EXCEPTION_START_CODE + 409);
define('RPC_RETURN_GONE',SELIFA_EXCEPTION_START_CODE + 410);

/**
 * Class BaseRPCController
 * @package RBS\Selifa\RPC
 */
class BaseRPCController implements IMiriaController
{
    #region Field and Protecteds
    /**
     * @var null|ReflectionClass
     */
    private $_R = null;

    /**
     * @var null|IMiriaExtension
     */
    protected $Extension = null;

    /**
     * @var array
     */
    protected $Options = array();

    /**
     * @var array
     */
    protected $RequestParam = array();

    /**
     * @var array
     */
    protected $CurrentParam = array();

    /**
     * @var string
     */
    protected $CurrentLanguage = 'en';

    /**
     * @var null|IMiriaAuthentication
     */
    protected $AuthenticationObject = null;

    /**
     * @var bool
     */
    protected $IsAuthenticated = false;

    /**
     * @var bool
     */
    protected $IsAuthorized = false;

    /**
     * @var int|string
     */
    protected $UserID = 0;

    /**
     * @var string
     */
    protected $ResultMessage = '';

    /**
     * @var int
     */
    protected $ResultCode = 0;

    /**
     * @var bool
     */
    protected $SkipOPTIONVerb = true;
    #endregion

    /**
     * @param string $source
     * @param string $tagName
     * @return array
     */
    private function ParseKeyValuePairs($source,$tagName)
    {
        $pattern = '#(?<=@'.$tagName.')([\s]+)([a-z0-9_.-]+)([\s]+)([\S ]+)#';
        $matches = null;
        preg_match_all($pattern,$source,$matches);

        $result = array();
        $keyCount = count($matches[2]);
        for ($i=0;$i<$keyCount;$i++)
        {
            if ($matches[4][$i] == 'true')
                $result[$matches[2][$i]] = true;
            else if ($matches[4][$i] == 'false')
                $result[$matches[2][$i]] = false;
            else
                $result[$matches[2][$i]] = $matches[4][$i];
        }

        return $result;
    }

    /**
     * @param ReflectionMethod $method
     * @param array $content
     * @throws SelifaException
     */
    private function _ExecuteMethod($method,&$content)
    {
        $isAuthenticationRequired = $this->GetParameterValue('auth',false);
        if ($isAuthenticationRequired && ($this->AuthenticationObject != null))
        {
            $authReturnException = $this->GetParameterValue('auth-return-exception',true);
            $isAuthenticated = $this->AuthenticationObject->ValidateSession();
            if (!$isAuthenticated)
            {
                if ($authReturnException)
                    throw new SelifaException(RPC_RETURN_FORBIDDEN);
            }

            $permission = $this->GetParameterValue('permission','');
            $isAuthorized = true;
            if ($permission != '')
            {
                $aPermissions = explode(',',$permission);
                $isAuthorized = $this->AuthenticationObject->ValidatePermissions($aPermissions);
                if (!$isAuthorized)
                {
                    if ($authReturnException)
                        throw new SelifaException(RPC_RETURN_UNAUTHORIZED);
                }
            }

            if ($isAuthenticated)
            {
                $this->UserID = $this->AuthenticationObject->GetUserID();
                $userData = $this->AuthenticationObject->GetUserData();
                foreach ($userData as $key => $value)
                    $this->_Globals[$key] = $value;
            }

            $this->OnAfterAuthentication($isAuthenticated,$isAuthorized);
            $this->IsAuthenticated = $isAuthenticated;
            $this->IsAuthorized = $isAuthorized;
        }

        $requestMP = $this->RequestParam['parameters'];
        $methodResult = $method->invokeArgs($this,$requestMP);
        if (is_array($methodResult))
            $content = array_replace_recursive($content,$methodResult);
        else if ($methodResult instanceof IMiriaControllerMethodResult)
        {
            $tResult = $methodResult->GetTranslatedResult();
            if (is_array($tResult))
                $content = array_replace_recursive($content,$tResult);
            else
                $content['Content'] = $tResult;
        }
        else
            $content['Content'] = $methodResult;
    }

    /**
     * @param bool $isSuccess
     * @param int $resultCode
     * @param string $eMessage
     * @param array|null $tData
     */
    protected function ProcessOutput($isSuccess,$resultCode,$eMessage,$tData)
    {
        $oData = array(
            'Success' => $isSuccess,
            'Message' => $eMessage,
            'ResultCode' => $resultCode
        );

        if (($tData != null) && is_array($tData))
            $oData = array_merge($oData,$tData);

        $jsonContentType = 'application/json';
        header('Content-Type: '.$jsonContentType);

        $hList = headers_list();

        $xPowered = '';
        foreach ($hList as $value)
        {
            $hS = explode(':',$value,2);
            $signature = strtolower(trim($hS[0]));
            if ($signature == 'x-powered-by')
            {
                $xPowered = trim($hS[1]);
                break;
            }
        }

        $sf = (SELIFA_NAME.' '.SELIFA);
        $xPowered = ($sf.($xPowered!=''?' - '.$xPowered:''));
        header('x-powered-by: '.$xPowered);
        header('x-runtime: '.SELIFA_NAME.' - RPC on Miria');
        header('x-version: '.SELIFA);

        $cancelOutput = false;
        $this->OnBeforeOutput($oData,$cancelOutput);
        $oData['ElapsedProcessingTime'] = round(Core::GetElapsedProcessingTime(),3);
        if (!$cancelOutput)
        {
            $responseText = json_encode($oData);
            $this->OnResponseFinalized($oData,$responseText);
            echo $responseText;
        }
    }

    #region IMiriaController Implementations
    /**
     * @return string
     */
    public function GetControllerTypeName()
    {
        return 'rpc';
    }

    /**
     * @param array $options
     * @param array $request
     * @param IMiriaExtension $extension
     */
    public function Initialize($options, $request, $extension)
    {
        $this->Extension = $extension;
        $this->Options = $options;
        $this->RequestParam = $request;

        if (isset($options['DefaultLanguage']))
            $this->CurrentLanguage = $options['DefaultLanguage'];

        if (isset($options['SkipOPTION']))
            $this->SkipOPTIONVerb = (bool)$options['SkipOPTION'];

        $this->_Globals['BaseURI'] = $this->RequestParam['baseuri'];
        $this->_R = new ReflectionClass($this);

        $this->OnInitializing($options);
    }

    /**
     *
     */
    public function Start()
    {
        //auth,permission,auth-return-exception,disable-output
        try
        {
            $methodName = $this->RequestParam['method'];
            $requestVerb = strtoupper($this->RequestParam['verb']);

            if (($requestVerb == 'OPTIONS') && ($this->SkipOPTIONVerb))
            {
                http_response_code(200);
                die();
            }

            $cDocs = array();
            $dClass = $this->_R;
            while (true)
            {
                if ($dClass->getShortName() != 'BaseRPCController')
                {
                    $temp = $dClass->getDocComment();
                    if ($temp !== false)
                        $cDocs[] = $temp;
                }

                $dClass = $dClass->getParentClass();
                if ($dClass == null)
                    break;
            }

            $dCount = count($cDocs);
            for ($i=$dCount-1;$i>=0;$i--)
            {
                $temp = $this->ParseKeyValuePairs($cDocs[$i],'rpc');
                $this->CurrentParam = array_replace_recursive($this->CurrentParam,$temp);
            }

            if (!$this->_R->hasMethod($methodName))
                throw new SelifaException(RPC_RETURN_NOT_FOUND, 'Method ' . $methodName . ' for '.$requestVerb.' does not exists.');
            $method = $this->_R->getMethod($methodName);

            $docComment = $method->getDocComment();
            $param = $this->ParseKeyValuePairs($docComment,'rpc');
            $this->CurrentParam = array_replace_recursive($this->CurrentParam,$param);

            $this->OnInitialized($methodName,$this->CurrentParam);

            $tContents = array();
            $this->_ExecuteMethod($method,$tContents);

            $this->OnAfterMethodExecuted($tContents,$this->_Globals,$this->CurrentParam);
            $isDisableOutput = (bool)$this->GetParameterValue('disable-output',false);
            if (!$isDisableOutput)
                $this->ProcessOutput(true,$this->ResultCode,$this->ResultMessage,$tContents);
        }
        catch (Exception $x)
        {
            $aData = array();
            if ($x instanceof SelifaException)
                $aData = $x->ArrayData;

            $eCode = $x->getCode();
            $eMessage = $x->getMessage();

            $this->OnProcessException($eCode,$eMessage,$aData);
            $this->ProcessOutput(false,$eCode,$eMessage,$aData);
        }
    }
    #endregion

    #region Helper Methods
    /**
     * @param string $key
     * @param mixed|null $defaultValue
     * @return mixed|boolean|null
     */
    protected function GetParameterValue($key,$defaultValue=null)
    {
        if (isset($this->CurrentParam[$key]))
            return $this->CurrentParam[$key];
        else
            return $defaultValue;
    }
    #endregion

    #region Extended Methods
    /**
     * @param array $options
     */
    protected function OnInitializing($options) { }

    /**
     * @param string $methodName
     * @param array $currentParam
     */
    protected function OnInitialized($methodName,&$currentParam) { }

    /**
     * @param boolean $isAuthenticated
     * @param boolean $isAuthorized
     */
    protected function OnAfterAuthentication(&$isAuthenticated,&$isAuthorized) { }

    /**
     * @param mixed $methodResult
     * @param array $globals
     * @param array $currentParam
     */
    protected function OnAfterMethodExecuted(&$methodResult,&$globals,&$currentParam) { }

    /**
     * @param string|int $eCode
     * @param string $eMessage
     * @param array $aData
     */
    protected function OnProcessException(&$eCode,&$eMessage,&$aData) { }

    /**
     * @param array $oData
     * @param bool $cancelOutput
     */
    protected function OnBeforeOutput(&$oData,&$cancelOutput) { }

    /**
     * @param array $responseData
     * @param string $responseText
     */
    protected function OnResponseFinalized(&$responseData,$responseText) { }
    #endregion
}
?>