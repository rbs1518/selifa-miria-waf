<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Miria;
use RBS\Selifa\Template\ITGXCustomRenderer;
use RBS\Selifa\Template\ParserManager;
use RBS\Selifa\Template\ITemplateParser;
use RBS\Selifa\Template\TGX;
use RBS\Selifa\Template\TGXAssetRegister;
use RBS\Selifa\XM;
use RBS\Utility\F;
use SelifaException, SelifaInternalException;
use Exception;
use ReflectionClass;
use ReflectionMethod;

define('MIRIA_CONTROLLER_TEMPLATE_NOT_FOUND',SELIFA_EXCEPTION_START_CODE + 501);

define('MIRIA_RETURN_BAD_REQUEST',SELIFA_EXCEPTION_START_CODE + 400);
define('MIRIA_RETURN_UNAUTHORIZED',SELIFA_EXCEPTION_START_CODE + 401);
define('MIRIA_RETURN_FORBIDDEN',SELIFA_EXCEPTION_START_CODE + 403);
define('MIRIA_RETURN_NOT_FOUND',SELIFA_EXCEPTION_START_CODE + 404);
define('MIRIA_RETURN_METHOD_NOT_ALLOWED',SELIFA_EXCEPTION_START_CODE + 405);
define('MIRIA_RETURN_NOT_ACCEPTABLE',SELIFA_EXCEPTION_START_CODE + 406);
define('MIRIA_RETURN_TIMEOUT',SELIFA_EXCEPTION_START_CODE + 408);
define('MIRIA_RETURN_CONFLICT',SELIFA_EXCEPTION_START_CODE + 409);
define('MIRIA_RETURN_GONE',SELIFA_EXCEPTION_START_CODE + 410);

/**
 * Class DefaultMiriaController
 * @package RBS\Selifa\Miria
 */
abstract class DefaultMiriaController implements IMiriaController, ITGXCustomRenderer
{
    #region Field and Protecteds
    /**
     * @var null|ReflectionClass
     */
    private $_R = null;

    /**
     * @var array
     */
    protected $_Globals = array(
        'UIMode' => 'default'
    );

    /**
     * @var null|IMiriaExtension
     */
    protected $Extension = null;

    /**
     * @var array
     */
    protected $Options = array();

    /**
     * @var array
     */
    protected $RequestParam = array();

    /**
     * @var array
     */
    protected $CurrentParam = array();

    /**
     * @var string
     */
    protected $CurrentLanguage = 'en';

    /**
     * @var null|IMiriaAuthentication
     */
    protected $AuthenticationObject = null;

    /**
     * @var bool
     */
    protected $EnableGlobalVariableDump = false;

    /**
     * @var bool
     */
    protected $EnableMasterVariableDump = false;

    /**
     * @var int|string
     */
    protected $UserID = 0;
    #endregion

    /**
     * @param string $source
     * @param string $tagName
     * @return array
     */
    private function ParseKeyValuePairs($source,$tagName)
    {
        $pattern = '#(?<=@'.$tagName.')([\s]+)([a-z0-9_.-]+)([\s]+)([\S ]+)#';
        $matches = null;
        preg_match_all($pattern,$source,$matches);

        $result = array();
        $keyCount = count($matches[2]);
        for ($i=0;$i<$keyCount;$i++)
        {
            if ($matches[4][$i] == 'true')
                $result[$matches[2][$i]] = true;
            else if ($matches[4][$i] == 'false')
                $result[$matches[2][$i]] = false;
            else
                $result[$matches[2][$i]] = $matches[4][$i];
        }

        return $result;
    }

    /**
     * @param ReflectionMethod $method
     * @param array $content
     * @throws SelifaException
     */
    private function _ExecuteMethod($method,&$content)
    {
        $isAuthenticationRequired = $this->GetParameterValue('auth',false);
        if ($isAuthenticationRequired && ($this->AuthenticationObject != null))
        {
            $this->_Globals['UseAuthentication'] = true;
            $authReturnException = $this->GetParameterValue('auth-return-exception',true);
            $isAuthenticated = $this->AuthenticationObject->ValidateSession();
            if (!$isAuthenticated)
            {
                if ($authReturnException)
                    throw new SelifaException(MIRIA_RETURN_FORBIDDEN);
            }

            $permission = $this->GetParameterValue('permission','');
            $isAuthorized = true;
            if ($permission != '')
            {
                $aPermissions = explode(',',$permission);
                $isAuthorized = $this->AuthenticationObject->ValidatePermissions($aPermissions);
                if (!$isAuthorized)
                {
                    if ($authReturnException)
                        throw new SelifaException(MIRIA_RETURN_UNAUTHORIZED);
                }
            }

            if ($isAuthenticated)
            {
                $this->UserID = $this->AuthenticationObject->GetUserID();
                $userData = $this->AuthenticationObject->GetUserData();
                foreach ($userData as $key => $value)
                    $this->_Globals[$key] = $value;
            }

            $this->OnAfterAuthentication($isAuthenticated,$isAuthorized);
            $this->_Globals['IsAuthenticated'] = $isAuthenticated;
            $this->_Globals['IsAuthorized'] = $isAuthorized;
        }

        $requestMP = $this->RequestParam['parameters'];
        $flagParamIndex = (count($requestMP) - 1);

        $this->_Globals['IsDataSubmitted'] = false;
        if (isset($_POST['_submitted']))
        {
            $requestMP[$flagParamIndex][] = 'data_submitted';
            $this->_Globals['IsDataSubmitted'] = true;
        }

        $methodResult = $method->invokeArgs($this,$requestMP);
        if (is_array($methodResult))
            $content = array_replace_recursive($content,$methodResult);
        else if ($methodResult instanceof IMiriaControllerMethodResult)
        {
            $tResult = $methodResult->GetTranslatedResult();
            if (is_array($tResult))
                $content = array_replace_recursive($content,$tResult);
            else
                $content['Content'] = $tResult;
        }
        else
            $content['Content'] = $methodResult;
    }

    /**
     * @param array $aContents
     * @throws SelifaException
     */
    private function _FinalProcess($aContents)
    {
        $cType = $this->RequestParam['ctype'];
        $templateFile = $this->GetParameterValue('html',true);
        if ($templateFile === true)
        {
            if (in_array($cType,array(MIRIA_CONTROLLER_ORGANIZED,MIRIA_CONTROLLER_NAMESPACED)))
                $templateFile = ($this->RequestParam['cpath'].'/'.$this->RequestParam['method'].'.html');
            else
                $templateFile = ($this->RequestParam['cpath'].'_'.$this->RequestParam['method'].'.html');
        }
        else if ($templateFile != '')
            $templateFile = $this->CurrentParam['html'];

        if ($this->EnableGlobalVariableDump)
        {
            F::StartOutputBuffer();
            var_dump($this->_Globals);
            $s = F::EndOutputBuffer();
            echo ('<pre>'.$s.'</pre>');
        }

        if ($this->EnableMasterVariableDump)
        {
            F::StartOutputBuffer();
            var_dump($aContents);
            $s = F::EndOutputBuffer();
            echo ('<pre>'.$s.'</pre>');
        }

        if ($templateFile !== false)
        {
            $pObject = $this->GetParserObject();
            if ($pObject instanceof TGX)
            {
                $pObject->RegisterCustomRenderer($this);
                TGXAssetRegister::SetPartPath($this->GetTemplatePath().'parts');
            }

            $overridePath = $this->GetParameterValue('override-template',false);
            if (!$overridePath)
                $templateFile = ($this->GetTemplatePath() . 'contents/' . $templateFile);

            $pObject->RegisterGlobal($this->_Globals);

            if (!file_exists($templateFile))
                throw new SelifaException(MIRIA_CONTROLLER_TEMPLATE_NOT_FOUND,'Specified template file (' . $templateFile . ') is not found.');

            $rawContentHTML = file_get_contents($templateFile);
            $parsedContent = $pObject->ParseText($rawContentHTML, $aContents, array(
                'name' => $templateFile
            ));

            $tMasterFile = $this->GetParameterValue('layout', '');
            if ($tMasterFile === false)
                echo $parsedContent;
            else
            {
                if (!$overridePath)
                {
                    if (($tMasterFile == '') && (isset($this->Options['MasterLayoutFile'])))
                        $tMasterFile = $this->Options['MasterLayoutFile'];
                    $tMasterFile = ($this->GetTemplatePath() . 'layouts/' . $tMasterFile);
                }

                /*if (($tMasterFile == '') && (isset($this->Options['MasterLayoutFile'])))
                    $tMasterFile = $this->Options['MasterLayoutFile'];
                $tMasterFile = ($this->GetTemplatePath() . 'layouts/' . $tMasterFile);*/

                if (!file_exists($tMasterFile))
                    throw new SelifaException(MIRIA_CONTROLLER_TEMPLATE_NOT_FOUND, 'Specified layout file (' . $tMasterFile . ') is not found.');

                $aContents['MainContent'] = $parsedContent;
                $rawLayoutHTML = file_get_contents($tMasterFile);

                echo $pObject->ParseText($rawLayoutHTML, $aContents, array(
                    'name' => $tMasterFile
                ));
            }
        }
        else
        {
            $isBinaryOutput = (bool)$this->GetParameterValue('binary-output',false);
            if (!$isBinaryOutput)
                echo $aContents;
            else
            {
                //binary output
            }
        }
    }

    #region IMiriaController Implementations
    /**
     * @return string
     */
    public function GetControllerTypeName()
    {
        return 'default';
    }

    /**
     * @param array $options
     * @param array $request
     * @param IMiriaExtension $extension
     */
    public function Initialize($options, $request, $extension)
    {
        $this->Extension = $extension;
        $this->Options = $options;
        $this->RequestParam = $request;

        if (isset($options['DefaultLanguage']))
            $this->CurrentLanguage = $options['DefaultLanguage'];

        $this->_Globals['BaseURI'] = $this->RequestParam['baseuri'];
        $this->_Globals['MiriaException'] = false;
        $this->_R = new ReflectionClass($this);

        $this->OnInitializing($options);
    }

    /**
     *
     */
    public function Start()
    {
        //title,html,auth,permission,layout,auth-return-exception,handle-exception,binary-output,disable-output
        try
        {
            $methodName = $this->RequestParam['method'];

            $cDocs = array();
            $dClass = $this->_R;
            while (true)
            {
                if ($dClass->getShortName() != 'DefaultMiriaController')
                {
                    $temp = $dClass->getDocComment();
                    if ($temp !== false)
                        $cDocs[] = $temp;
                }

                $dClass = $dClass->getParentClass();
                if ($dClass == null)
                    break;
            }

            $dCount = count($cDocs);
            for ($i=$dCount-1;$i>=0;$i--)
            {
                $temp = $this->ParseKeyValuePairs($cDocs[$i],'miria');
                $this->CurrentParam = array_replace_recursive($this->CurrentParam,$temp);
            }

            if (!$this->_R->hasMethod($methodName))
                throw new SelifaException(MIRIA_RETURN_NOT_FOUND,'Method '.$methodName.' does not exists.');
            $method = $this->_R->getMethod($methodName);

            $docComment = $method->getDocComment();
            $param = $this->ParseKeyValuePairs($docComment,'miria');
            $this->CurrentParam = array_replace_recursive($this->CurrentParam,$param);

            $this->OnInitialized($methodName,$this->CurrentParam);
            $this->_Globals['PageTitle'] = $this->GetParameterValue('title','');

            $tContents = array();
            $isHandleException = $this->GetParameterValue('handle-exception',false);
            if (!$isHandleException)
                $this->_ExecuteMethod($method,$tContents);
            else
            {
                try { $this->_ExecuteMethod($method,$tContents); }
                catch (Exception $x)
                {
                    $this->_Globals['UIMode'] = 'exception';
                    $this->_Globals['MiriaException'] = true;
                    if (($x instanceof SelifaException) || ($x instanceof SelifaInternalException))
                    {
                        $x->AppendData(array(
                            'CurrentParam' => $this->CurrentParam,
                            'RequestParam' => $this->RequestParam
                        ));
                        $result = XM::HandleException($x);
                        $this->_Globals['ExceptionCode'] = $result['C'];
                        $this->_Globals['ExceptionMessage'] = $result['M'];
                    }
                    else
                    {
                        $this->_Globals['ExceptionMessage'] = $x->getMessage();
                    }
                }
            }

            $this->OnAfterMethodExecuted($tContents,$this->_Globals,$this->CurrentParam);

            $isDisableOutput = (bool)$this->GetParameterValue('disable-output',false);
            if (!$isDisableOutput)
                $this->_FinalProcess($tContents);
        }
        catch (Exception $x)
        {
            $aData = array();
            if ($x instanceof SelifaException)
                $aData = $x->ArrayData;

            $doDie = true;
            $this->OnProcessException($x->getCode(),$x->getMessage(),$aData,$doDie);
            if ($doDie)
                die('Miria Controller Exception ('.$x->getCode().'), '.$x->getMessage());
        }
    }
    #endregion

    #region Helper Methods
    /**
     * @param string $key
     * @param mixed|null $defaultValue
     * @return mixed|boolean|null
     */
    protected function GetParameterValue($key,$defaultValue=null)
    {
        if (isset($this->CurrentParam[$key]))
            return $this->CurrentParam[$key];
        else
            return $defaultValue;
    }

    /**
     * @param string $key
     * @param mixed $value
     */
    protected function SetGlobal($key,$value)
    {
        $this->_Globals[$key] = $value;
    }

    /**
     * @param string $to
     * @return string
     */
    protected function CreateURI($to='')
    {
        $baseURI = $this->RequestParam['baseuri'];
        return ($baseURI.$to);
    }

    /**
     * @param array $arrayOfString
     * @return string
     */
    protected function CreateConcatenatedURI($arrayOfString)
    {
        $baseURI = $this->RequestParam['baseuri'];
        return ($baseURI.implode('/',$arrayOfString));
    }

    /**
     * @return string
     */
    protected function GetTemplatePath()
    {
        if (isset($this->Options['TemplatePath']))
            return (SELIFA_ROOT_PATH.'/'.$this->Options['TemplatePath'].$this->CurrentLanguage.'/');
        else
            return (SELIFA_ROOT_PATH.'/'.$this->CurrentLanguage.'/');
    }

    /**
     * @return ITemplateParser
     * @throws Exception
     */
    protected function GetParserObject()
    {
        $parserClass = $this->Options['Parser'];
        return ParserManager::Get($parserClass);
    }

    /**
     * @param string $to
     * @param bool $stopProcess
     */
    protected function RedirectTo($to,$stopProcess=false)
    {
        $toURI = $this->CreateURI($to);
        header('Location: '.$toURI);
        if ($stopProcess)
            die();
    }

    #region Extended Methods
    /**
     * @param array $options
     */
    protected function OnInitializing($options) { }

    /**
     * @param string $methodName
     * @param array $currentParam
     */
    protected function OnInitialized($methodName,&$currentParam) { }

    /**
     * @param boolean $isAuthenticated
     * @param boolean $isAuthorized
     */
    protected function OnAfterAuthentication(&$isAuthenticated,&$isAuthorized) { }

    /**
     * @param mixed $methodResult
     * @param array $globals
     * @param array $currentParam
     */
    protected function OnAfterMethodExecuted(&$methodResult,&$globals,&$currentParam) { }

    /**
     * @param string|int $eCode
     * @param string $eMessage
     * @param array $aData
     * @param boolean $doDie
     */
    protected function OnProcessException($eCode,$eMessage,$aData,&$doDie) { }
    #endregion

    #region ITGXCustomRenderer Implementations
    /**
     * @param TGX $tgxObject
     * @return mixed
     */
    public function RegisterRenderer($tgxObject)
    {
        // TODO: Implement RegisterRenderer() method.
    }

    /**
     * @return array
     */
    public function GetRenderFunctionNames()
    {
        return array('ref','cref','hschars');
    }

    /**
     * @param string $functionName
     * @param array $parameters
     * @param boolean $isInverted
     * @param array $vars
     * @return mixed|null
     */
    public function RenderInline($functionName, $parameters, $isInverted, $vars)
    {
        if (($functionName == 'ref') && (isset($parameters[0])))
            return $this->CreateURI($parameters[0]);
        else if (($functionName == 'cref') && (count($parameters) > 0))
            return $this->CreateConcatenatedURI($parameters);
        else if (($functionName == 'hschars') && (isset($parameters[0])))
            return htmlspecialchars($parameters[0],ENT_COMPAT|ENT_HTML401);
        return '';
    }

    /**
     * @param string $functionName
     * @param array $parameters
     * @param array $token
     * @param boolean $isInverted
     * @param array $vars
     * @return mixed|null
     */
    public function RenderBlock($functionName, $parameters, $token, $isInverted, $vars)
    {
        return false;
    }
    #endregion
}
?>