<?php
namespace RBS\Selifa\Miria;
use RBS\Selifa\Core;
use RBS\Selifa\RPC\ILanguageDriverObject;
use RBS\Utility\F, Exception;
use ReflectionClass;

/**
 * Class DMERPCController
 * @package RBS\Selifa\RPC
 */
class DMEMiriaController extends DefaultMiriaController
{
    /**
     * @var bool
     */
    protected $IsDeveloperMode = false;

    /**
     * @var string
     */
    protected $DeveloperModeAct = '';

    /**
     * @var string
     */
    protected $TargetLanguage = '';

    /**
     * @var string
     */
    protected $OutputType = 'json';

    /**
     * @var null|ILanguageDriverObject
     */
    protected $LDO = null;

    /**
     * @param array $options
     * @throws Exception
     */
    protected function OnInitializing($options)
    {
        if (isset($_SERVER['HTTP_X_DEVELOPER']))
        {
            $dmOptString = trim($_SERVER['HTTP_X_DEVELOPER']);
            $dmOpts = json_decode($dmOptString, true);

            $isEnabled = (bool)F::Extract($dmOpts, 'enabled', false);
            $dMode = (bool)F::Extract($options,'DeveloperMode',false);
            $isEnabled = ($isEnabled && $dMode);

            $this->IsDeveloperMode = $isEnabled;
            if ($isEnabled)
            {
                $this->DeveloperModeAct = strtolower(trim(F::Extract($dmOpts, 'act', '')));
                $this->TargetLanguage = strtolower(trim(F::Extract($dmOpts, 'language', '')));

                $dmlPath = (SELIFA_DRIVER_PATH.'dml/'.$this->TargetLanguage.'.php');
                if (!file_exists($dmlPath))
                    throw new Exception('LDO file for '.$this->TargetLanguage.' is not found.');
                include_once($dmlPath);

                $className = ($this->TargetLanguage.'LDO');
                $ldoObject = new $className();
                if (!($ldoObject instanceof ILanguageDriverObject))
                    throw new Exception('LDO file for '.$this->TargetLanguage.' or its parent does not implement ILanguageDriverObject.');

                $this->OutputType = strtolower(trim(F::Extract($dmOpts,'output','json')));

                $ldoObject->SetOptions($dmOpts);
                $this->LDO = $ldoObject;
            }
        }
    }

    protected function OnBeforeOutput(&$oData, &$cancelOutput)
    {
        if ($this->IsDeveloperMode)
        {
            $cancelOutput = true;
            $ldoRC = new ReflectionClass($this->LDO);
            if ($ldoRC->hasMethod($this->DeveloperModeAct))
            {
                $method = $ldoRC->getMethod($this->DeveloperModeAct);
                $result = $method->invokeArgs($this->LDO,array(
                    $oData,
                    $this->_Globals
                ));
                if ($result !== null)
                {
                    switch ($this->OutputType)
                    {
                        case 'json':
                            $oData = array('Success'=>true,'Message'=>'','ResultCode'=>0);
                            if (($result != null) && is_array($result))
                                $oData = array_merge($oData,$result);

                            $jsonContentType = 'application/json';
                            header('Content-Type: '.$jsonContentType);

                            $hList = headers_list();
                            $xPowered = '';
                            foreach ($hList as $value)
                            {
                                $hS = explode(':',$value,2);
                                $signature = strtolower(trim($hS[0]));
                                if ($signature == 'x-powered-by')
                                {
                                    $xPowered = trim($hS[1]);
                                    break;
                                }
                            }

                            $sf = (SELIFA_NAME.' '.SELIFA);
                            $xPowered = ($sf.($xPowered!=''?' - '.$xPowered:''));
                            header('x-powered-by: '.$xPowered);
                            header('x-runtime: '.SELIFA_NAME.' - RPC on Miria [Developer]');
                            header('x-version: '.SELIFA);

                            $oData['ElapsedProcessingTime'] = round(Core::GetElapsedProcessingTime(),3);
                            echo json_encode($oData);
                            break;
                        case 'raw':
                            header('Content-Type: text/plain');
                            echo $result;
                            break;
                        case 'dump':
                            header('Content-Type: text/plain');
                            var_dump($result);
                            break;
                    }
                }
            }
        }
    }
}
?>