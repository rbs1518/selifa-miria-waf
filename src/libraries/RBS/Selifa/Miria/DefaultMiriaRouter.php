<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Miria;

define('MIRIA_ROUTING_NORMAL',0);
define('MIRIA_ROUTING_STAR_AS_KORONA',1);

/**
 * Class DefaultMiriaRouter
 * @package RBS\Selifa\Miria
 */
class DefaultMiriaRouter implements IMiriaRouting
{
    /**
     * @var null|Miria
     */
    private $_Parent = null;

    /**
     * @var array
     */
    private $_Aliases = array(
        ':digit' => '[0-9]+',
        ':char' => '[A-Za-z]+',
        ':alpha' => '[A-Za-z0-9]+',
        ':std' => '[A-Za-z0-9_\-\.]+',
        ':hex' => '[0-9A-Fa-f]+'
    );

    /**
     * @var array
     */
    private $_ParsedRules = array();

    /**
     * @var bool
     */
    private $_IsDefaultStarDefined = false;

    /**
     * @var null|string
     */
    private $_DefaultControllerPath = null;

    /**
     * @var array|null
     */
    private $_DefaultVerbs = null;

    /**
     * @var int
     */
    private $_RouteType = MIRIA_ROUTING_NORMAL;

    /**
     * @param string $pathQuery
     * @param string $classPath
     * @param string $className
     * @param string $methodName
     * @return bool
     */
    private function _TranslateKoronaPath($pathQuery,&$classPath,&$className,&$methodName)
    {
        $parts = explode('/', $pathQuery);
        $lastIndex = (count($parts) - 1);
        if ($lastIndex == 0)
        {
            $classPath = '';
            $className = $parts[0];
            $methodName = 'main';
            return true;
        }
        else
        {
            if ($lastIndex == 1)
            {
                $classPath = '';
                $className = $parts[0];
                $methodName = $parts[1];
                return true;
            }
            else
            {
                if ($lastIndex > 1)
                {
                    $methodName = $parts[$lastIndex];
                    $className = $parts[$lastIndex - 1];
                    unset($parts[$lastIndex]);
                    unset($parts[$lastIndex - 1]);
                    $classPath = (implode('/', $parts) . '/');
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @param int $routeType
     */
    public function __construct($routeType=MIRIA_ROUTING_NORMAL)
    {
        $this->_RouteType = $routeType;
    }

    /**
     * @param Miria $parent
     * @param array $options
     * @param array $ruleArray
     */
    public function DefineRules(Miria $parent, $options, $ruleArray)
    {
        $this->_Parent = $parent;
        $x = $parent->GetRegisteredExtension();
        if ($x != null)
            $x->OnRuleDefine($ruleArray,$options);

        foreach ($ruleArray as $ruleKey => $value)
        {
            $cVerbs = null;
            $cPath = $value;

            /*$cPath = 'main/main';
            if (is_array($value))
            {
                if (count($value) >= 2)
                {
                    $cPath = $value[0];
                    $cVerbs = explode(',',$value[1]);
                }
            }
            else
                $cPath = $value;*/

            if ($ruleKey == '*')
            {
                if ($this->_RouteType == MIRIA_ROUTING_NORMAL)
                {
                    $parts = explode('/',$ruleKey);
                    $partCount = count($parts);

                    $varIndexes = array();
                    $phCidx = 0;
                    $starIndex = -1;

                    for ($i=0;$i<$partCount;$i++)
                    {
                        if (isset($this->_Aliases[$parts[$i]]))
                        {
                            $parts[$i] = ('('.$this->_Aliases[$parts[$i]].')');
                            $varIndexes[] = ($phCidx + 1);
                            $phCidx++;
                        }
                        else if (substr($parts[$i],0,2) == '::')
                        {
                            $parts[$i] = ('('.substr($parts[$i],2).')');
                            $varIndexes[] = ($phCidx + 1);
                            $phCidx++;
                        }
                        else if ($parts[$i] == '*')
                        {
                            $parts[$i] = '([\S]+)';
                            $starIndex = ($phCidx + 1);
                            $phCidx++;
                        }
                    }

                    $pRule = implode('/',$parts);
                    $this->_ParsedRules[$pRule] = array(
                        'count' => $partCount,
                        'vars' => $varIndexes,
                        'star' => $starIndex,
                        'verbs' => $cVerbs,
                        'multiverb' => is_array($cPath),
                        'path' => $cPath
                    );
                }
                else if ($this->_RouteType == MIRIA_ROUTING_STAR_AS_KORONA)
                {
                    $this->_IsDefaultStarDefined = true;
                    if ($value == '')
                        $this->_DefaultControllerPath = '';
                    else
                        $this->_DefaultControllerPath = ($value.'/');
                    $this->_DefaultVerbs = $cVerbs;
                }
            }
            else
            {
                $parts = explode('/',$ruleKey);
                $partCount = count($parts);

                $varIndexes = array();
                $phCidx = 0;
                $starIndex = -1;

                for ($i=0;$i<$partCount;$i++)
                {
                    if (isset($this->_Aliases[$parts[$i]]))
                    {
                        $parts[$i] = ('('.$this->_Aliases[$parts[$i]].')');
                        $varIndexes[] = ($phCidx + 1);
                        $phCidx++;
                    }
                    else if (substr($parts[$i],0,2) == '::')
                    {
                        $parts[$i] = ('('.substr($parts[$i],2).')');
                        $varIndexes[] = ($phCidx + 1);
                        $phCidx++;
                    }
                    else if ($parts[$i] == '*')
                    {
                        $parts[$i] = '([\S]+)';
                        $starIndex = ($phCidx + 1);
                        $phCidx++;
                    }
                }

                $pRule = implode('/',$parts);
                $this->_ParsedRules[$pRule] = array(
                    'count' => $partCount,
                    'vars' => $varIndexes,
                    'star' => $starIndex,
                    'verbs' => $cVerbs,
                    'multiverb' => is_array($cPath),
                    'path' => $cPath
                );
            }
        }

        if ($x != null)
            $x->OnRuleDefined($this->_ParsedRules);
    }

    /**
     * @param string $queryString
     * @param string $requestVerb
     * @return array|null
     */
    public function MatchQueryString($queryString, $requestVerb)
    {
        $x = $this->_Parent->GetRegisteredExtension();
        if ($x != null)
            $x->OnRuleMatch($this,$queryString,$requestVerb);

        $matchedResult = null;
        foreach ($this->_ParsedRules as $key => $spec)
        {
            $matches = array();
            $matchCount = preg_match('@^' . $key . '$@', $queryString, $matches);
            if ($matchCount > 0)
            {
                if ($spec['multiverb'])
                {
                    $rVerb = strtoupper(trim($requestVerb));
                    if (isset($spec['path'][$rVerb]))
                    {
                        $spec['path'] = $spec['path'][$rVerb];
                        $matchedResult = array('matches'=>$matches,'spec'=>$spec);
                        break;
                    }
                }
                else
                {
                    $matchedResult = array('matches'=>$matches,'spec'=>$spec);
                    break;
                }
            }
        }

        if ($matchedResult != null)
        {
            $captures = array();
            foreach ($matchedResult['spec']['vars'] as $mIndex)
                $captures[] = $matchedResult['matches'][$mIndex];

            $specPath = $matchedResult['spec']['path'];
            $result = array('path'=>'','name'=>'main','method'=>'main','vars'=>$captures,'flags'=>null);
            $starIndex = $matchedResult['spec']['star'];
            if ($starIndex >= 0)
            {
                $starParts = explode('/',$matchedResult['matches'][$starIndex]);
                $result['method'] = (count($starParts)>0?$starParts[0]:'main');
                $result['flags'] = array_diff($starParts,array($result['method']));
                $result['verb'] = $requestVerb;

                $splath = explode('/',$specPath);
                $spc = count($splath);
                if ($spc > 1)
                {
                    $result['name'] = $splath[$spc-1];
                    if ($result['name'] == '')
                        $result['name'] = 'main';
                    unset($splath[$spc-1]);
                    $result['path'] = implode('/',$splath);
                }
                else if ($spc == 1)
                    $result['name'] = $splath[0];

                if (($result['path'] != '') && (substr($result['path'],-1,1) != '/'))
                    $result['path'] .= '/';
                if ($x != null)
                    $x->OnRuleMatched($this,$queryString,$requestVerb,$result);
                return $result;
            }
            else
            {
                $b = $this->_TranslateKoronaPath($specPath,$result['path'],$result['name'],$result['method']);
                if ($b)
                {
                    if (($result['path'] != '') && (substr($result['path'],-1,1) != '/'))
                        $result['path'] .= '/';
                    if ($x != null)
                        $x->OnRuleMatched($this,$queryString,$requestVerb,$result);
                    return $result;
                }
            }
        }
        else
        {
            if ($this->_IsDefaultStarDefined)
            {
                $cPath = ($this->_DefaultControllerPath.$queryString);
                $result = array('path'=>'','name'=>'main','method'=>'main','verb'=>$requestVerb,'vars'=>array(),'flags'=>null);
                $b = $this->_TranslateKoronaPath($cPath,$result['path'],$result['name'],$result['method']);
                if ($b)
                {
                    if (($result['path'] != '') && (substr($result['path'],-1,1) != '/'))
                        $result['path'] .= '/';
                    if ($x != null)
                        $x->OnRuleMatched($this,$queryString,$requestVerb,$result);
                    return $result;
                }
            }
        }

        $result = null;
        if ($x != null)
            $x->OnUnmatchedRule($this,$queryString,$requestVerb,$result);
        return $result;
    }
}
?>
