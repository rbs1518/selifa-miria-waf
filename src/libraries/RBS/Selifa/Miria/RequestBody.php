<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Miria;
use RBS\System\ITransferableObject;

/**
 * Class RequestBody
 * @package RBS\Selifa\RPC
 */
class RequestBody implements ITransferableObject
{
    /**
     * @var null|RequestBody
     */
    private static $_Instance = null;

    /**
     * @var null|array
     */
    private $_Data = null;

    /**
     * @var bool
     */
    public $UseJSONBody = false;

    /**
     * @var string
     */
    public $RawData = '';

    /**
     *
     */
    private function __construct()
    {
        $contentType = strtolower(trim($_SERVER['CONTENT_TYPE']));
        if (($contentType == 'text/json') || ($contentType == 'application/json'))
            $this->UseJSONBody = true;

        if ($this->UseJSONBody)
            $this->_Data = json_decode(file_get_contents('php://input'),true);
        else
        {
            $rMethod = strtoupper(trim($_SERVER['REQUEST_METHOD']));
            if (($rMethod == 'PUT') || ($rMethod == 'PATCH'))
            {
                $var = file_get_contents('php://input');
                $this->RawData = $var;
                parse_str($var,$this->_Data);
            }
            else
            {
                $this->_Data = $_POST;
            }
        }
    }

    /**
     * @return null|RequestBody
     */
    public static function Initialize()
    {
        if (self::$_Instance == null)
            self::$_Instance = new RequestBody();
        return self::$_Instance;
    }

    /**
     * @return null|RequestBody
     */
    public static function Instance()
    {
        return self::$_Instance;
    }

    /**
     * @param string $key
     * @param mixed|null $default
     * @return mixed|null
     */
    public static function GetData($key,$default=null)
    {
        if (self::$_Instance != null)
        {
            if (isset(self::$_Instance->_Data[$key]))
                return self::$_Instance->_Data[$key];
            else
                return $default;
        }

        return $default;
    }

    /**
     * @param string $name
     * @return string|mixed
     */
    public function __get($name)
    {
        if (isset($this->_Data[$name]))
            return $this->_Data[$name];
        else
            return '';
    }

    #region ITransferableObject implementation
    /**
     * @return array
     */
    public function GetTransferableMembers()
    {
        $result = array();
        foreach ($this->_Data as $key => $value)
        {
            $result[] = array(
                'Name' => $key,
                'DataType' => 'string'
            );
        }
        return $result;
    }

    /**
     * @param ITransferableObject|array $tObjectOrArray
     */
    public function To(&$tObjectOrArray)
    {
        if ($tObjectOrArray instanceof ITransferableObject)
        {
            $tMembers = $tObjectOrArray->GetTransferableMembers();
            foreach ($tMembers as $m)
            {
                if (isset($this->_Data[$m['Name']]))
                {
                    $value = $this->_Data[$m['Name']];
                    if ($m['DataType'] != 'bool')
                        $tObjectOrArray->{$m['Name']} = $value;
                    else
                    {
                        if (is_bool($value))
                            $tObjectOrArray->{$m['Name']} = $value;
                        else
                            $tObjectOrArray->{$m['Name']} = ($value == '1');
                    }
                }
            }
        }
        else if (is_array($tObjectOrArray))
        {
            foreach ($this->_Data as $key => $value)
                $tObjectOrArray[$key] = $value;
        }
    }

    /**
     * @return array
     */
    public function ToArray()
    {
        return $this->_Data;
    }

    /**
     * @param ITransferableObject|array $tObjectOrArray
     */
    public function From($tObjectOrArray)
    {
        //Not implemented, because none should modify REST input content.
    }
    #endregion
}
?>