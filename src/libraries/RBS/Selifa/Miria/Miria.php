<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Miria;
use Exception;
use RBS\Selifa\IComponent;

define('MIRIA_CONTROLLER_GLOBAL',0);
define('MIRIA_CONTROLLER_ORGANIZED',1);
define('MIRIA_CONTROLLER_NAMESPACED',2);

define('MIRIA_GENERAL_ERROR',SELIFA_EXCEPTION_SYSTEM_START_CODE + 901);
define('MIRIA_DISPATCHER_ERROR',SELIFA_EXCEPTION_SYSTEM_START_CODE + 902);

#region Interfaces
/**
 * Interface IMiriaRouting
 * @package RBS\Selifa\Miria
 */
interface IMiriaRouting
{
    /**
     * @param Miria $parent
     * @param array $options
     * @param array $ruleArray
     */
    public function DefineRules(Miria $parent, $options, $ruleArray);

    /**
     * @param string $queryString
     * @param string $requestVerb
     * @return array|null
     */
    public function MatchQueryString($queryString, $requestVerb);
}

/**
 * Interface IMiriaExtension
 * @package RBS\Selifa\Miria
 */
interface IMiriaExtension
{
    /**
     * @param array $ruleDefinitions
     * @param array $options
     */
    public function OnRuleDefine(&$ruleDefinitions,&$options);

    /**
     * @param array $parsedRules
     */
    public function OnRuleDefined(&$parsedRules);

    /**
     * @param IMiriaRouting $router
     * @param string $queryString
     * @param string $requestVerb
     */
    public function OnRuleMatch(IMiriaRouting $router, &$queryString, &$requestVerb);

    /**
     * @param IMiriaRouting $router
     * @param string $queryString
     * @param string $requestVerb
     * @param array $routeInfo
     */
    public function OnRuleMatched(IMiriaRouting $router, $queryString, $requestVerb, &$routeInfo);

    /**
     * @param IMiriaRouting $router
     * @param string $queryString
     * @param string $requestVerb
     * @param array $routeInfo
     */
    public function OnUnmatchedRule(IMiriaRouting $router, $queryString, $requestVerb, &$routeInfo);

    /**
     * @param string $filePath
     * @param string $className
     * @param string $method
     * @param array $parameters
     */
    public function OnControllerTranslated($filePath,$className,&$method,&$parameters);

    /**
     * @param IMiriaController $controller
     * @return mixed
     */
    public function OnControllerDispatched(IMiriaController $controller);

    /**
     * @param string $filePath
     * @param string $className
     * @param string $errorCode
     * @param string $errorMessage
     * @param boolean $handled
     * @return mixed
     */
    public function OnControllerDispatchError($filePath,$className,$errorCode,$errorMessage,&$handled);
}

/**
 * Interface IMiriaController
 * @package RBS\Selifa\Miria
 */
interface IMiriaController
{
    /**
     * @return string
     */
    public function GetControllerTypeName();

    /**
     * @param array $options
     * @param array $request
     * @param IMiriaExtension $extension
     */
    public function Initialize($options,$request,$extension);

    /**
     *
     */
    public function Start();
}
#endregion

class Miria implements IComponent
{
    #region Fields
    /**
     * @var null|Miria
     */
    private static $_Instance = null;

    /**
     * @var array
     */
    private $_Config = array();

    /**
     * @var null|IMiriaExtension
     */
    private $X = null;

    /**
     * @var null|IMiriaRouting
     */
    private $_Router = null;

    /**
     * @var int
     */
    private $_ControllerType = MIRIA_CONTROLLER_ORGANIZED;

    #endregion

    /**
     * @param array $options
     */
    private function __construct($options)
    {
        $currentPath = (dirname(__FILE__).'/');
        $this->_Config = $options;
    }

    /**
     * @param array $ruleArray
     * @param int $controllerType
     */
    private function _DefineRule($ruleArray,$controllerType)
    {
        if ($this->_Router == null)
            $this->_Router = new DefaultMiriaRouter();
        $this->_Router->DefineRules($this,$this->_Config,$ruleArray);
    }

    /**
     * @return string
     */
    public function GetBasePath()
    {
        if (isset($this->_Config['BaseURI']))
        {
            if (trim($this->_Config['BaseURI']) != '')
                return $this->_Config['BaseURI'];
        }
        else if (isset($_SERVER['HTTP_APP_BASE_URI']))
            return $_SERVER['HTTP_APP_BASE_URI'];

        $protos = 'http';
        if (isset($_SERVER['HTTPS']))
            $protos = 'https';
        else if (isset($_SERVER['HTTP_X_SCHEME']))
            $protos = strtolower($_SERVER['HTTP_X_SCHEME']);
        else if (isset($_SERVER['HTTP_X_FORWARDED_PROTO']))
            $protos = strtolower($_SERVER['HTTP_X_FORWARDED_PROTO']);
        else if (isset($_SERVER['SERVER_PORT']))
        {
            $serverPort = (int)$_SERVER['SERVER_PORT'];
            if ($serverPort == 80)
                $protos = 'http';
            else if ($serverPort == 443)
                $protos = 'https';
        }
        return ($protos."://".$_SERVER['HTTP_HOST']."/");
    }

    /**
     * @param array $routeInfo
     * @return null|IMiriaController
     * @throws Exception
     */
    private function _DispatchController($routeInfo)
    {
        $suffix = $this->_Config['ControllerSuffix'];
        $parameters = $routeInfo['vars'];
        $parameters[] = ($routeInfo['flags']!=null?$routeInfo['flags']:array());

        if ($this->_ControllerType == MIRIA_CONTROLLER_GLOBAL)
        {
            $controllerPath = (str_replace('/','_',$routeInfo['path']).$routeInfo['name']);
            $className = ($controllerPath.'_'.$suffix);
        }
        else if ($this->_ControllerType == MIRIA_CONTROLLER_NAMESPACED)
        {
            $controllerPath = ($routeInfo['path'].$routeInfo['name']);
            $className = (str_replace('/',"\\",$routeInfo['path']).$routeInfo['name'].$suffix);
        }
        else
        {
            $controllerPath = ($routeInfo['path'].$routeInfo['name']);
            $className = ($routeInfo['name'].$suffix);
        }

        if (isset($routeInfo['nsprefix']))
            $className = ($routeInfo['nsprefix'].$className);

        $dNamespace = '';
        if (isset($this->_Config['DefaultNamespace']))
            $dNamespace = $this->_Config['DefaultNamespace'];
        if ($dNamespace != '')
            $className = ($dNamespace."\\".$className);

        $filePath = ($controllerPath.'.php');
        if (isset($routeInfo['base']))
            $realFilePath = (SELIFA_ROOT_PATH.'/'.$routeInfo['base'].$this->_Config['ControllerPath'].$filePath);
        else
            $realFilePath = (SELIFA_ROOT_PATH.'/'.$this->_Config['ControllerPath'].$filePath);

        if ($this->X != null)
            $this->X->OnControllerTranslated($realFilePath,$className,$routeInfo['method'],$parameters);

        try
        {
            if (!file_exists($realFilePath))
                throw new Exception('Controller file '.$filePath.' is not found.');
            require_once($realFilePath);

            $cObject = new $className();
            if (!($cObject instanceof IMiriaController))
                throw new Exception('Controller class '.$className.' or its parent does not implement IMiriaController.');

            $typeName = $cObject->GetControllerTypeName();
            $cOptions = array();
            if (isset($this->_Config['ControllerOptions'][$typeName]))
                $cOptions = $this->_Config['ControllerOptions'][$typeName];

            $reqParams = array(
                'baseuri' => $routeInfo['baseuri'],
                'ctype' => $this->_ControllerType,
                'cpath' => $controllerPath,
                'method' => $routeInfo['method'],
                'parameters' => $parameters,
                'flags' => $routeInfo['flags'],
                'verb' => $routeInfo['verb']
            );

            if (isset($routeInfo['extra']))
                foreach ($routeInfo['extra'] as $key => $value)
                    $reqParams[$key] = $value;

            $cObject->Initialize($cOptions,$reqParams,$this->X);
            if ($this->X != null)
                $this->X->OnControllerDispatched($cObject);
            return $cObject;
        }
        catch (Exception $x)
        {
            if ($this->X != null)
            {
                $handled = false;
                $this->X->OnControllerDispatchError($realFilePath,$className,MIRIA_DISPATCHER_ERROR,$x->getMessage(),$handled);
                if (!$handled)
                    throw new Exception($x->getMessage());
            }
            else
                throw new Exception($x->getMessage());
        }

        return null;
    }

    /**
     * @return null|IMiriaExtension
     */
    public function GetRegisteredExtension()
    {
        return $this->X;
    }

    #region Statics
    /**
     * @param array $options
     * @return Miria|null
     */
    public static function Initialize($options)
    {
        if (self::$_Instance == null)
            self::$_Instance = new Miria($options);
        return self::$_Instance;
    }

    /**
     * Register an extension to Miria Engine.
     * @param IMiriaExtension $extension
     */
    public static function RegisterExtension(IMiriaExtension $extension)
    {
        self::$_Instance->X = $extension;
    }

    /**
     * @param IMiriaRouting $router
     */
    public static function RegisterRouter(IMiriaRouting $router)
    {
        self::$_Instance->_Router = $router;
    }

    /**
     * @param array $ruleArray
     * @param int $controllerType
     */
    public static function Define($ruleArray,$controllerType=MIRIA_CONTROLLER_ORGANIZED)
    {
        self::$_Instance->_ControllerType = $controllerType;
        self::$_Instance->_DefineRule($ruleArray,$controllerType);
    }

    /**
     * @throws Exception
     */
    public static function Start()
    {
        $i = self::$_Instance;
        $requestVerb = strtolower($_SERVER['REQUEST_METHOD']);
        $queryString = 'main/main';
        if (isset($_GET['query']))
        {
            $queryString = trim($_GET['query']);
            if (substr($queryString, -1, 1) == '/')
                $queryString = substr($queryString, 0, -1);
        }

        $routeInfo = $i->_Router->MatchQueryString($queryString,$requestVerb);
        if ($routeInfo != null)
        {
            $routeInfo['found'] = true;
            $routeInfo['verb'] = $requestVerb;
        }
        else
            $routeInfo['found'] = false;

        $routeInfo['baseuri'] = $i->GetBasePath();
        $cObject = $i->_DispatchController($routeInfo);
        $cObject->Start();
    }
    #endregion

    #region Test Functions
    public static function Test_RouteMatch($qsArray,$withVerb)
    {
        echo "Miria RouteMatch test started.\n";
        foreach ($qsArray as $testItem)
        {
            echo ("Item: ".$testItem."\n");
            $route = self::$_Instance->_Router->MatchQueryString($testItem,$withVerb);
            var_dump($route);
            /*$cObject = self::$_Instance->_DispatchController($route);
            var_dump($cObject);*/
            echo "\n\n";
        }
        echo "Miria RouteMatch test finished.\n";
    }
    #endregion
}
?>