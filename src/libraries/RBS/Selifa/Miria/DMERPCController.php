<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Miria;
use RBS\Selifa\Core;
use RBS\Utility\F, Exception;
use ReflectionClass;

/**
 * Class DMERPCController
 * @package RBS\Selifa\RPC
 */
class DMERPCController extends BaseRPCController
{
    /**
     * @var bool
     */
    protected $IsDeveloperMode = false;

    /**
     * @var string
     */
    protected $DeveloperModeAct = '';

    /**
     * @var string
     */
    protected $TargetLanguage = '';

    /**
     * @var string
     */
    protected $OutputType = 'json';

    /**
     * @var null|ILanguageDriverObject
     */
    protected $LDO = null;

    /**
     * @param array $options
     * @throws Exception
     */
    protected function OnInitializing($options)
    {
        if (isset($_SERVER['HTTP_X_DEVELOPER']))
        {
            $dmOptString = trim($_SERVER['HTTP_X_DEVELOPER']);
            $dmOpts = json_decode($dmOptString, true);

            $isEnabled = (bool)F::Extract($dmOpts, 'enabled', false);
            $this->IsDeveloperMode = $isEnabled;
            if ($isEnabled)
            {
                $this->DeveloperModeAct = strtolower(trim(F::Extract($dmOpts, 'act', '')));
                $this->TargetLanguage = strtolower(trim(F::Extract($dmOpts, 'language', '')));

                $dmlPath = (SELIFA_DRIVER_PATH.'dml/'.$this->TargetLanguage.'.php');
                if (!file_exists($dmlPath))
                    throw new Exception('LDO file for '.$this->TargetLanguage.' is not found.');
                include_once($dmlPath);

                $className = ($this->TargetLanguage.'LDO');
                $ldoObject = new $className();
                if (!($ldoObject instanceof ILanguageDriverObject))
                    throw new Exception('LDO file for '.$this->TargetLanguage.' or its parent does not implement ILanguageDriverObject.');

                $this->OutputType = strtolower(trim(F::Extract($dmOpts,'output','json')));

                $ldoObject->SetOptions($dmOpts);
                $this->LDO = $ldoObject;
            }
        }
    }

    protected function OnBeforeOutput(&$oData, &$cancelOutput)
    {
        if ($this->IsDeveloperMode)
        {
            $cancelOutput = true;
            $ldoRC = new ReflectionClass($this->LDO);
            if ($ldoRC->hasMethod($this->DeveloperModeAct))
            {
                $method = $ldoRC->getMethod($this->DeveloperModeAct);
                $jsonString = json_encode($oData);
                $renderedData = json_decode($jsonString,true);

                $result = $method->invokeArgs($this->LDO,array(
                    $renderedData,
                ));
                if ($result !== null)
                {
                    switch ($this->OutputType)
                    {
                        case 'json':
                            $oData = array('Success'=>true,'Message'=>'','ResultCode'=>0);
                            if (($result != null) && is_array($result))
                                $oData = array_merge($oData,$result);

                            $jsonContentType = 'application/json';
                            header('Content-Type: '.$jsonContentType);

                            $hList = headers_list();
                            $xPowered = '';
                            foreach ($hList as $value)
                            {
                                $hS = explode(':',$value,2);
                                $signature = strtolower(trim($hS[0]));
                                if ($signature == 'x-powered-by')
                                {
                                    $xPowered = trim($hS[1]);
                                    break;
                                }
                            }

                            $sf = (SELIFA_NAME.' '.SELIFA);
                            $xPowered = ($sf.($xPowered!=''?' - '.$xPowered:''));
                            header('x-powered-by: '.$xPowered);
                            header('x-runtime: '.SELIFA_NAME.' - RPC on Miria [Developer]');
                            header('x-version: '.SELIFA);

                            $oData['ElapsedProcessingTime'] = round(Core::GetElapsedProcessingTime(),3);
                            echo json_encode($oData);
                            break;
                        case 'raw':
                            header('Content-Type: text/plain');
                            echo $result;
                            break;
                        case 'dump':
                            header('Content-Type: text/plain');
                            var_dump($result);
                            break;
                    }
                }
            }
        }
    }
}
?>